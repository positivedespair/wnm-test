![](download.png)


![](download (1).png)


![](download (2).png)


![](download (3).png)


![](download (4).png)

Из построенных графиков и методом внимательного вглядывания в данные заметила следующие закономерности:

Количество вовлеченных пользователей(engaged users) страницы падает в выходные практически еженедельно, иногда довольно-таки значительно.

Оплаченный охват страницы превышает органический в несколько раз.

В предновогоднюю неделю количество лайков повышается, с максимумом в 53 лайка 23го декабря. Среднее число лайков за эту неделю примерно в полтора раза превышает таковое за все предыдущее время. При этом оплаченный охват страницы "упал" максимально.

7 октября наблюдается значительный скачок в общем охвате страницы, вызванный в большей мере рекордным(35955) за данный нам период оплаченным охватом.

16 ноября же наблюдается пик органического охвата(6557 уникальных пользователей), а также топ-2 значение вовлеченности пользователей.

Вовлеченность коррелирует в большей степени с органическим охватом, а не с оплаченным(с общим, в таком случае, также).


Основные тенденции, которые хотелось бы отметить:
   - Оплаченный охват хуже генерирует вовлеченность, нежели органический.
   - Значения подпадают под недельную "цикличность".